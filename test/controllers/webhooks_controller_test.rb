# frozen_string_literal: true

require "test_helper"

module StripeEventLite
  class WebhooksControllerTest < ActionDispatch::IntegrationTest
    include ActiveSupport::Testing::FileFixtures
    include Engine.routes.url_helpers

    setup do
      @routes = Engine.routes

      @secret1 = "secret1"
      @secret2 = "secret2"

      @evt_customer_created = JSON.parse(file_fixture("evt_customer_created.json").read)

      StripeEventLite.configure do |config|
        config.signing_secrets = [@secret1, @secret2]
      end
    end

    def post_webhook(params, signature)
      post root_path, params:, as: :json, headers: { 'HTTP_STRIPE_SIGNATURE' => signature }
    end

    def post_webhook_with_signature(params, secret = @secret1)
      post_webhook params, generate_signature(params, secret)
    end

    def generate_signature(params, secret)
      payload   = params.to_json
      timestamp = Time.now.utc
      signature = Stripe::Webhook::Signature.compute_signature(timestamp, payload, secret)

      "t=#{timestamp.to_i},v1=#{signature}"
    end

    test "processes event with correct signature" do
      post_webhook_with_signature(@evt_customer_created)
      assert_response :success
    end

    test "rejects event with invalid signature" do
      post_webhook(@evt_customer_created, "invalid signature")
      assert_response :bad_request
    end

    test "rejects event with invalid signing secret" do
      post_webhook(@evt_customer_created, generate_signature(@evt_customer_created, "invalid secret"))
      assert_response :bad_request
    end

    test "calls correct event handler" do
      count = 0
      StripeEventLite.configuration.register "customer.created" do
        count += 1
      end

      post_webhook_with_signature(@evt_customer_created)
      assert_response :success

      assert_equal 1, count
    end

    test "call error handler when signature is invalid" do
      count = 0
      StripeEventLite.configuration.on_error = -> (error) { count +=1 }

      post_webhook(@evt_customer_created, "invalid signature")
      assert_response :bad_request

      assert_equal 1, count
    end
  end
end
