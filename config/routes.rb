StripeEventLite::Engine.routes.draw do
  root to: "webhooks#event", via: :post
end
