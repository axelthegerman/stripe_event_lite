module StripeEventLite
  class WebhooksController < ActionController::Base
    if Rails.application.config.action_controller.default_protect_from_forgery
      skip_before_action :verify_authenticity_token
    end

    def event
      payload = request.body.read
      signature = request.headers["HTTP_STRIPE_SIGNATURE"]

      StripeEventLite.process(payload, signature)

      head :ok
    rescue StripeEventLite::Error => e
      Rails.logger.error(e)

      StripeEventLite.configuration.on_error&.call(e)

      head :bad_request
    end
  end
end
