require "stripe_event_lite/version"
require "stripe_event_lite/engine"

require "stripe_event_lite/configuration"
require "stripe_event_lite/event_handler"

require "stripe_event_lite/engine" if defined?(Rails)

require "stripe"

module StripeEventLite
  class Error < StandardError; end;
  class SignatureVerificationError < Error; end

  class << self
    attr_reader :configuration

    def configure(&block)
      raise ArgumentError, "must provide a block" unless block_given?

      @configuration ||= Configuration.new

      block.arity.zero? ? instance_eval(&block) : yield(configuration)
    end

    def process(payload, signature)
      EventHandler.new.process(payload, signature)
    end
  end
end
