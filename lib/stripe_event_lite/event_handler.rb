module StripeEventLite
  class EventHandler
    def process(payload, signature)
      signed_event = construct_event(payload, signature)

      handler = StripeEventLite.configuration.handlers[signed_event.type]
      handler ||= StripeEventLite.configuration.on_missing_handler

      handler&.call(signed_event)
    end

    private

    def construct_event(payload, signature)
      possible_secrets = StripeEventLite.configuration.signing_secrets
      raise SignatureVerificationError, "Missing webhook signing secret configuration." unless possible_secrets

      possible_secrets.each_with_index do |secret, i|
        return Stripe::Webhook.construct_event(
          payload,
          signature,
          secret
        )
      rescue Stripe::SignatureVerificationError
        raise SignatureVerificationError if i == possible_secrets.size - 1
        next
      end
    end
  end
end
