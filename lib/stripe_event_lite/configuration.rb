module StripeEventLite
  class Configuration
    attr_reader :handlers, :signing_secrets
    attr_accessor :on_error, :on_missing_handler

    def initialize
      @handlers = {}
    end

    def signing_secrets=(value)
      @signing_secrets = Array(value)
    end

    def register(topic, callable = nil, &block)
      callable ||= block
      @handlers[topic] = callable
    end
  end
end
