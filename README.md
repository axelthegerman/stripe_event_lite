# StripeEventLite

Adaptation of [stripe_event](https://rubygems.org/gems/stripe_event) gem designed to be even more simplistic.

## How is it different from `stripe_event`?

- higher minimum `stripe` gem requirement - [v5.19.0](https://github.com/stripe/stripe-ruby/blob/master/CHANGELOG.md#5190---2020-04-24) exposed the webhook signature check method publicly
- no restriction on newer `stripe` gem versions - assuming Stripe won't break the new public message without notice
- can be used without `Rails` and `ActiveSupport`

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem "stripe_event_lite"
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install stripe_event_lite
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
