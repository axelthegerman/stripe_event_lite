require_relative "lib/stripe_event_lite/version"

Gem::Specification.new do |spec|
  spec.name        = "stripe_event_lite"
  spec.version     = StripeEventLite::VERSION
  spec.authors     = ["Axel Gustav"]
  spec.email       = ["dev@axelgustav.de"]
  spec.homepage    = "https://gitlab.com/AxelTheGerman/stripe_event_lite"
  spec.summary     = "Lightweight adaptation of stripe_event gem."
  spec.description = "Simplistic helper to process Stripe webhooks - with and without Rails."

  spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "stripe", ">= 5.19.0"

  spec.add_development_dependency "rails", ">= 7.1.3.2"
end
